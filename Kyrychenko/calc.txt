using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace simple_calc
{
    class Program
    {
        static void Main(string[] args)
        {
         double num_1, num_2, num_3;
         char ch;
         
         Console.WriteLine("Input first num: ");
        num_1 = Convert.ToDouble(Console.ReadLine());
         Console.WriteLine("Input second num: ");
        num_2 = Convert.ToDouble(Console.ReadLine());
         Console.WriteLine("Input operand: ");
        ch = Convert.ToChar(Console.ReadLine());
      
            if (ch == '*') { 
                num_3 = num_1 * num_2;
                Console.WriteLine(num_1 + "" + ch + "" + num_2 + "=" + num_3 + ".");
            }
            
            else if (ch == '-'){ 
                num_3 = num_1 - num_2;
                Console.WriteLine(num_1 + "" + ch + "" + num_2 + "=" + num_3 + ".");
                
            }
            
            else if (ch == '+'){ 
                num_3 = num_1 + num_2;
                Console.WriteLine(num_1 + "" + ch + "" + num_2 + "=" + num_3 + ".");
            }
            
            else if (ch == '%'){ 
                num_3 = num_1 % num_2;
                Console.WriteLine(num_1 + "" + ch + "" + num_2 + "=" + num_3 + ".");
            }
            else {
                Console.WriteLine("Error");
            }

        }
    }
}